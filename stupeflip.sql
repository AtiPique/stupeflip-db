-- MariaDB dump 10.19-11.2.2-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: stupeflip
-- ------------------------------------------------------
-- Server version	11.2.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `idAlbum` int(11) DEFAULT NULL,
  `nomAlbum` varchar(20) DEFAULT NULL,
  `anneeSortie` year(4) DEFAULT NULL,
  `duree` time DEFAULT NULL,
  `nbChansons` int(11) DEFAULT NULL,
  `nbPersos` int(11) DEFAULT NULL,
  `nbInterpretes` int(11) DEFAULT NULL,
  `idInterpretes` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` VALUES
(1,'Stupeflip',2003,'01:02:28',22,894,6,'1, 2, 3, 4, 5, 8'),
(2,'Stup Religion',2005,'00:53:00',20,894,6,'1, 2, 3, 6, 7, 8'),
(3,'Hypnoflip Invasion',2011,'01:03:20',21,894,4,'1, 2, 3, 8'),
(4,'Stup Virus',2017,'01:01:02',21,894,5,'1, 2, 3, 9, 8'),
(5,'Stup Forever',2022,'00:48:06',19,894,4,'1, 2, 3, 8');
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chansons`
--

DROP TABLE IF EXISTS `chansons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chansons` (
  `idChanson` int(11) DEFAULT NULL,
  `numChanson` int(11) DEFAULT NULL,
  `nomChanson` varchar(20) DEFAULT NULL,
  `idAlbum` int(11) DEFAULT NULL,
  `idPersos` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chansons`
--

LOCK TABLES `chansons` WRITE;
/*!40000 ALTER TABLE `chansons` DISABLE KEYS */;
/*!40000 ALTER TABLE `chansons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interpretes`
--

DROP TABLE IF EXISTS `interpretes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interpretes` (
  `idInterprete` int(11) DEFAULT NULL,
  `nomInterprete` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interpretes`
--

LOCK TABLES `interpretes` WRITE;
/*!40000 ALTER TABLE `interpretes` DISABLE KEYS */;
INSERT INTO `interpretes` VALUES
(1,'Julien Barthelemy'),
(2,'Stéphane Béllenger'),
(3,'Jean-Paul Michel'),
(4,'Denis Quillard'),
(5,'Mangu'),
(6,'Hélène'),
(7,'Louise'),
(8,'Inconnu'),
(9,'Colette');
/*!40000 ALTER TABLE `interpretes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personnages`
--

DROP TABLE IF EXISTS `personnages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personnages` (
  `personnage` varchar(40) DEFAULT NULL,
  `idInterprete` int(11) DEFAULT NULL,
  `idPerso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personnages`
--

LOCK TABLES `personnages` WRITE;
/*!40000 ALTER TABLE `personnages` DISABLE KEYS */;
INSERT INTO `personnages` VALUES
('Cadillac / Casimor',2,1),
('King Ju',1,2),
('Pop Hip',1,3),
('Mc Salo',3,4),
('Reverb Man',1,5),
('Sandrine Chacheton',8,6),
('Stup',2,7),
('Flip',1,8),
('Joel',1,9),
('Fabien Pollet',1,10),
('Le membre chaché du Stupeflip Crou',2,11),
('Hélène',6,12),
('Louise',7,13),
('Jacno',4,14),
('Mangu',5,15),
('Colette',9,16);
/*!40000 ALTER TABLE `personnages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-07  0:45:34
